package com.karahan.calculator;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class CalculatorTest {

    Calculator calculator;

    @Before
    public void setUp() {
        calculator = new Calculator();
    }
    @Test
    public void should_return_zero_when_input_is_empty() {
        //given
        String input = "";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void should_return_number_when_input_is_a_single_number() {
        //given
        String input = "1";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void should_return_sum_when_input_is_two_numbers() {
        //given
        String input = "1,2";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(3);
    }

    @Test
    public void should_return_sum_when_input_is_more_than_two_numbers() {
        //given
        String input = "1,2,3,4,5,6,7";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(28);
    }

    @Test
    public void should_return_sum_when_delimiter_is_newline() {
        //given
        String input = "1\n2,3";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(6);
    }

    @Test
    public void should_return_sum_when_delimiter_is_different_than_comma() {
        //given
        String input = "//;\n1;2";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(3);
    }

    @Test
    public void should_throw_exception_when_given_negative_numbers() {
        //given
        String input = "-1,5,-3";

        //when & then
        assertThatThrownBy(() -> calculator.add(input))
                .isInstanceOf(NegativeNumberException.class)
                .hasMessage("negatives not allowed: -1 -3");
    }

    @Test
    public void should_ignore_numbers_bigger_than_1000_when_adding_numbers() {
        //given
        String input = "2,1001";

        //when
        int result = calculator.add(input);

        //then
        assertThat(result).isEqualTo(2);
    }
}
