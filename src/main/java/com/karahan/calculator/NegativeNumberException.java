package com.karahan.calculator;

public class NegativeNumberException extends RuntimeException {

    public NegativeNumberException (String message) {
        super(message);
    }
}
