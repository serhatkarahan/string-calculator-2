package com.karahan.calculator;

import java.util.ArrayList;

public class Calculator {

    private static final String DELIMITER_IDENTIFIER = "//";

    public int add(String input) {

        if (input.isEmpty()) {
            return 0;
        }
        String delimiterRegex = createDelimiterRegex(input);
        String numberString = createNumberString(input);

        ArrayList<Integer> numbers = parseNumbersToAdd(delimiterRegex, numberString);
        checkIfNegativeNumbersExist(numbers);
        ArrayList<Integer> numbersLessThan1000 = filterNumbersBiggerThan1000(numbers);
        return addNumbers(numbersLessThan1000);
    }

    private String createDelimiterRegex(String input) {
        String delimiterRegex = "[,\n]";
        if (input.contains(DELIMITER_IDENTIFIER)) {
            String newDelimiter = extractDelimiter(input);
            delimiterRegex = "[" + newDelimiter + "\n]";
        }
        return delimiterRegex;
    }

    private String extractDelimiter(String input) {
        int delimiterStartIndex = DELIMITER_IDENTIFIER.length();
        return input.substring(delimiterStartIndex, delimiterStartIndex + 1);
    }

    private String createNumberString(String input) {
        String numberString = input;
        if (input.contains(DELIMITER_IDENTIFIER)) {
            numberString = input.substring(4);
        }
        return numberString;
    }

    private ArrayList<Integer> parseNumbersToAdd(String delimiterRegex, String numberString) {
        ArrayList<Integer> numbersToAdd = new ArrayList<>();
        String[] numberStringList = numberString.split(delimiterRegex);
        for (String stringToBeInt : numberStringList) {
            numbersToAdd.add(stringToInt(stringToBeInt));
        }
        return numbersToAdd;
    }

    private int stringToInt(String input) {
        return Integer.parseInt(input);
    }

    private void checkIfNegativeNumbersExist(ArrayList<Integer> numbers) {
        ArrayList<Integer> negativeNumbers = retrieveNegativeNumbers(numbers);
        if (!negativeNumbers.isEmpty()) {
            String message = "negatives not allowed:";
            for (Integer negativeNumber : negativeNumbers) {
                message += " " + negativeNumber.toString();
            }
            throw new NegativeNumberException(message);
        }
    }

    private ArrayList<Integer> retrieveNegativeNumbers(ArrayList<Integer> numbers) {
        ArrayList<Integer> negativeNumbers = new ArrayList<>();
        for (int number : numbers) {
            if (number < 0) {
                negativeNumbers.add(number);
            }
        }
        return negativeNumbers;
    }

    private ArrayList<Integer> filterNumbersBiggerThan1000(ArrayList<Integer> numbers) {
        ArrayList<Integer> numbersLessThan1000 = new ArrayList<>();
        for (int number : numbers) {
            if (number <= 1000) {
                numbersLessThan1000.add(number);
            }
        }
        return numbersLessThan1000;
    }

    private int addNumbers(ArrayList<Integer> numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }
}
